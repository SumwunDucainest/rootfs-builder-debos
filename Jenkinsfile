@Library("ubports-build-tools") _

String cron_string = BRANCH_NAME == 'master' ? 'H 3 * * *' : ''

def build_image = {
  sh '''
    ./debos-podman \
      "./$RECIPE" \
      --fakemachine-backend kvm \
      -m 12G \
      --cpus $(nproc --all) \
      -t "architecture:${ARCHITECTURE}" \
      -t "ut_version_major:${UT_VERSION_MAJOR}" \
      -t "image:${IMAGE}"
  '''
  sh 'if [ "$POST_BUILD_XZ" = "1" ]; then xz -1f -Q --threads 0 "$IMAGE"; fi'
  sh 'echo "$(date -u +%Y-%m-%dT%H:%M:%SZ)_${ARCHITECTURE}_${GIT_BRANCH}_${BUILD_NUMBER}_${GIT_COMMIT}" | tee "${IMAGE}.build"'
  archiveArtifacts(artifacts: '*.tar.gz,*.xz,*.build,*.manifest', fingerprint: true, onlyIfSuccessful: true)
}

def check_for_changes = {
  sh 'git branch --force master $(git show-ref -s origin/master)'
  return sh (
    returnStatus: true,
    script: '! git diff --quiet $GIT_COMMIT origin/master -- ubuntu-touch/ scripts/ common/ Jenkinsfile'
  )
}

pipeline {
  agent none
  triggers {
    cron(cron_string)
  }
  options {
    buildDiscarder(logRotator(artifactNumToKeepStr: '30', numToKeepStr: '180'))
  }
  stages {
    stage('Rootfs builds') {
      matrix {
        agent none
        axes {
          axis {
            name 'VARIANT'
            values 'ubuntu-touch-hybris-rootfs', 'ubuntu-touch-android9plus-rootfs', 'ubuntu-touch-pdk-img', 'ubuntu-touch-pinephone-pro-img', 'ubuntu-touch-pinephone-img', 'ubuntu-touch-pinetab-img', 'ubuntu-touch-pinetab2-img'
          }

          axis {
            name 'ARCHITECTURE'
            values 'arm64', 'armhf', 'amd64'
          }

          axis {
            name 'UT_VERSION_MAJOR'
            values 'focal', 'devel'
          }
        }
        excludes {
          exclude {
              axis {
                  name 'VARIANT'
                  notValues 'ubuntu-touch-pdk-img'
              }
              axis {
                  name 'ARCHITECTURE'
                  values 'amd64'
              }
          }
          exclude {
              axis {
                  name 'VARIANT'
                  values 'ubuntu-touch-pdk-img', 'ubuntu-touch-pinephone-pro-img', 'ubuntu-touch-pinephone-img', 'ubuntu-touch-pinetab-img', 'ubuntu-touch-pinetab2-img'
              }
              axis {
                  name 'ARCHITECTURE'
                  values 'armhf'
              }
          }
          exclude {
            axis {
              name 'VARIANT'
              notValues 'ubuntu-touch-hybris-rootfs', 'ubuntu-touch-android9plus-rootfs', 'ubuntu-touch-pdk-img'
            }
            axis {
              name 'UT_VERSION_MAJOR'
              notValues 'focal'
            }
          }
        }

        stages {
          stage("Debos") {
            agent { label 'debos-amd64' } // Yes, ARM images are built on AMD64 too.
            when {
              beforeAgent false
              anyOf {
                branch 'master'
                expression { check_for_changes() == 0 }
              }
            }

            environment {
              RECIPE = "ubuntu-touch/${VARIANT}.yaml"
              IMAGE = "${VARIANT}${UT_VERSION_MAJOR == "focal" ? "" : "-${UT_VERSION_MAJOR}"}-${ARCHITECTURE}.${VARIANT.endsWith("-rootfs") ? "tar.gz" : "raw"}"
              ARCHITECTURE = "${ARCHITECTURE}"
              UT_VERSION_MAJOR = "${UT_VERSION_MAJOR}"
              POST_BUILD_XZ = "${VARIANT.endsWith("-img") ? "1" : ""}"
            }
            steps {
              script {build_image()}
            }
            post {
              cleanup {
                deleteDir()
              }
            }
          }
        }
      }
    }
  }
  post {
    always {
      node(null /* means any node */) {
        script {
          notifyBuildStatus(
            currentBuild,
            /* jobDescription */ "Ubuntu Touch rootfs builds",
            /* jobUrl */ env.RUN_DISPLAY_URL,
            /* wantTelegramNotification */ env.BRANCH_NAME == 'master'
          );
        }
      }
    }
  }
}
